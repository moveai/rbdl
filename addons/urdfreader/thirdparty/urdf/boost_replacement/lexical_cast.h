#ifndef BOOST_REPLACEMENT_LEXICAL_CAST_H
#define BOOST_REPLACEMENT_LEXICAL_CAST_H

#include <stdlib.h>
#include <sstream>

namespace boost
{

	template <typename T> T lexical_cast(const char* txt)
	{
		//atof fails on some PC environment. 3.14 ==> 3 or 0.14 ==> 0
		//double result = atof(txt);
		//return result;
		std::stringstream ss;
		ss << txt;
		double result;
		ss >> result;
		return result;
	};

	struct bad_lexical_cast
	{
		const char* what()
		{
		return ("bad lexical cast\n");
		}
		
	};
	
} //namespace boost

#endif

